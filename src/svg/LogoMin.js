import React from 'react';

const LogoMin = () => {
	return (
		<svg
			width="25"
			height="30"
			viewBox="0 0 25 30"
			fill="none"
			xmlns="http://www.w3.org/2000/svg"
		>
			<path
				d="M6.06585 0H24.0566V5.99792H6.06585V29.6488L12.1317 23.6509H17.9218H24.0566V29.717H6.06585L12.1317 23.6509L6.06585 29.6488L0 23.6509V5.99792L6.06585 0Z"
				fill="#034561"
			/>
			<path d="M24.0567 30L18.1133 23.7736H24.0567V30Z" fill="#1C819E" />
			<path
				d="M11.8867 18.1132V11.8868H18.1131V18.1132H11.8867Z"
				fill="#FFBE00"
			/>
		</svg>
	);
};

export default LogoMin;

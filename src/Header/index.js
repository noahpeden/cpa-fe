import React from 'react'
import { withRouter } from 'react-router-dom'
import { graphql, compose } from 'react-apollo'
import { Link } from 'react-router-dom'
import gql from 'graphql-tag'
import styled from 'react-emotion'
import LogoSmall from '../svg/LogoSmall'

const HeaderContainer = styled('header')`
	height: 100px;
	max-width: 100%;
	font-size: 24px;
	padding: 20px;
	display: flex;
	justify-content: space-between;
	a {
		text-decoration: none;
		color: #fdbd2c;
	}
`

class Header extends React.Component {
	state = {
		top: false,
		userId: ''
	}

	onLogout = async e => {
		e.preventDefault()
		localStorage.removeItem('graphcoolToken')
		this.props.history.replace('/')
		window.location.reload()
	}

	componentWillMount() {
		const userId = localStorage.getItem('graphcoolToken') || ''
		if (this.state.userId !== userId) {
			this.setState({ userId })
		}
	}

	render() {
		const { userId } = this.state

		return (
			<HeaderContainer>
				<LogoSmall />
				<Link to="/how-it-works">How It Works</Link>
				<Link to="pick">Help Me Pick</Link>
				<Link to="/resources">Resources</Link>

				{userId ? (
					<Link to="/" onClick={this.onLogout}>Log Out</Link>
				) : (
					<React.Fragment>
						<Link to="/login">Log In</Link>{' '}
						<Link to="/signup">Sign Up</Link>
					</React.Fragment>
				)}
			</HeaderContainer>
		)
	}
}

const AUTH_INFO_QUERY = gql`
	query AuthInfo {
		authInfo @client {
			userID
			token
		}
	}
`

const UPDATE_AUTH_INFO_MUTATION = gql`
	mutation UpdateAuthInfo($userID: String, $token: String) {
		updateAuthInfo(userID: $userID, token: $token) @client
	}
`

const LOGGED_IN_USER = gql`
	query loggedInUser {
		loggedInUser {
			id
		}
	}
`

export default compose(
	graphql(LOGGED_IN_USER, {
		props: ({ data: { loggedInUser } }) => ({
			id: loggedInUser ? loggedInUser.id : null
		})
	}),
	graphql(AUTH_INFO_QUERY, {
		props: ({ data: { authInfo } }) => ({
			authInfo
		})
	}),
	graphql(UPDATE_AUTH_INFO_MUTATION, {
		props: ({ mutate }) => ({
			updateAuthInfo: variables => {
				return mutate({ variables })
			}
		})
	})
)(withRouter(Header))

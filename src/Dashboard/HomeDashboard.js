import React from 'react'
import { withRouter } from 'react-router-dom'
import { graphql, compose } from 'react-apollo'
import { Link } from 'react-router-dom'
import gql from 'graphql-tag'
import styled from 'react-emotion'
import Header from '../Header';

const UserCardContainer = styled('div')`
    height: 800px;
    display: flex;
    flex-direction: row;
    width: 80%;
    margin: 80px auto;
    border: 1px solid black;
    flex-wrap: wrap;
`

const UserBoxes = styled('div')`
    border: 1px solid blue;
    height: 160px;
    width: 26%;
    margin: 10px auto;
`

class HomeDashboard extends React.Component {
	state = {
		userId: ''
	}
	componentWillMount() {
		const userId = localStorage.getItem('graphcoolToken') || ''
		if (this.state.userId !== userId) {
			this.setState({ userId })
		}
		if (userId === '') {
			this.props.history.push('/')
		}
	}
	render() {
		return <React.Fragment>
            <Header/>
            <UserCardContainer>
                <UserBoxes></UserBoxes>
                <UserBoxes></UserBoxes>
                <UserBoxes></UserBoxes>
                <UserBoxes></UserBoxes>
                <UserBoxes></UserBoxes>
                <UserBoxes></UserBoxes>
                <UserBoxes></UserBoxes>
            </UserCardContainer>
            This is the user dashboard.
            </React.Fragment>
	}
}

const AUTH_INFO_QUERY = gql`
	query AuthInfo {
		authInfo @client {
			userID
			token
		}
	}
`

const UPDATE_AUTH_INFO_MUTATION = gql`
	mutation UpdateAuthInfo($userID: String, $token: String) {
		updateAuthInfo(userID: $userID, token: $token) @client
	}
`

const LOGGED_IN_USER = gql`
	query loggedInUser {
		loggedInUser {
			id
		}
	}
`

export default compose(
	graphql(LOGGED_IN_USER, {
		props: ({ data: { loggedInUser } }) => ({
			id: loggedInUser ? loggedInUser.id : null
		})
	}),
	graphql(AUTH_INFO_QUERY, {
		props: ({ data: { authInfo } }) => ({
			authInfo
		})
	}),
	graphql(UPDATE_AUTH_INFO_MUTATION, {
		props: ({ mutate }) => ({
			updateAuthInfo: variables => {
				return mutate({ variables })
			}
		})
	})
)(withRouter(HomeDashboard))

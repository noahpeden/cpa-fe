import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import App from './App'
import { ApolloClient } from 'apollo-client'
import { ApolloProvider } from 'react-apollo'
import { createHttpLink } from 'apollo-link-http'
import { ApolloLink } from 'apollo-link'
import { InMemoryCache } from 'apollo-cache-inmemory'
import 'tachyons'
import './index.css'
import CreateUser from './Auth/CreateUser'
import CreateLogin from './Auth/CreateLogin'
import HomeDashboard from './Dashboard/HomeDashboard';

// __SIMPLE_API_ENDPOINT__ looks like: 'https://api.graph.cool/simple/v1/__SERVICE_ID__'
const httpLink = createHttpLink({
	uri: 'https://api.graph.cool/simple/v1/cjm6we1oz2za90111pn006jsk'
})

const client = new ApolloClient({
	link: httpLink,
	cache: new InMemoryCache().restore(window.__APOLLO_STATE__)
})

const middlewareLink = new ApolloLink((operation, forward) => {
	const token = localStorage.getItem('graphcoolToken')
	const authorizationHeader = token ? `Bearer ${token}` : null
	operation.setContext({
		headers: {
			authorization: authorizationHeader
		}
	})
	return forward(operation)
})

const httpLinkWithAuthToken = middlewareLink.concat(httpLink)

ReactDOM.render(
	<ApolloProvider client={client}>
		<Router>
			<React.Fragment>
				<Route exact path="/" component={App} />
				<Route path="/signup" component={CreateUser} />
				<Route path="/login" component={CreateLogin} />
				<Route path="/dashboard" component={HomeDashboard} />
			</React.Fragment>
		</Router>
	</ApolloProvider>,
	document.getElementById('root')
)

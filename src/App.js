import React, { Component } from 'react'
import Header from './Header'
import styled from 'react-emotion'
		const MainContainer = styled('main')`
			font-family: 'Rubik', 'sans-serif';
        
		`

		const HeroSection = styled('div')`
			display: flex;
		`
		const CallToAction = styled('button')`
			border: 1px solid #094560;
			outline: none;
            
			color: #fdbd2c;
		`

class App extends Component {
	render() {
		return (
			<MainContainer>
				<Header />
				<HeroSection className="hero-section">
					<section className="left-hand">
						<h1>Customized Programs Abroad</h1>
						<CallToAction>Find a Program Provider</CallToAction>
						<CallToAction>Get a Bid Now</CallToAction>
						<p>
							We are an independent and easy-to-use directory of
							customized program providers for faculty, advisors,
							and education abroad leaders.
						</p>
						<p>
							A customzied program is an abroad program tailored
							tomeet the needs of your group, which can include:
						</p>
						<ul>
							<li>Faculty led programs</li>
							<li>Existing provider program integration</li>
							<li>Service Learning projects</li>
							<li>Host country faculty instruction</li>
							<li>Internships abroad</li>
						</ul>
					</section>
					<section className="right-hand">
						<img src="https://bit.ly/2E8102h" alt="" />
					</section>
				</HeroSection>
				<div className="find-provider">
					<h1>Find the best provider for your needs</h1>
					<div>Program Location</div>
					<div>Duraction / Term</div>
					<div>Program Subject</div>
					<div>Program Type</div>
				</div>
			</MainContainer>
		)
	}
}

export default App

import React from 'react'
import styled from 'react-emotion'
import gql from 'graphql-tag'
import { withRouter } from 'react-router-dom'
import { graphql, compose } from 'react-apollo'

const FormContainer = styled('form')`
	display: flex;
	justify-content: center;
	flex-flow: column;
	margin: 25%;
`

class CreateUser extends React.Component {
	state = {
		email: '',
		password: '',
		firstname: '',
		lastname: '',
		user: false,
		provider: false
	}

	signupUser = async event => {
		event.preventDefault()
        const { email, password, firstname, lastname, user, provider } = this.state
        let isProvider;
        provider ? isProvider = true : isProvider = false
		try {
			const user = await this.props.signupUserMutation({
				variables: {
					email,
					password,
					firstname,
                    lastname,
                    isProvider
				}
			})
			console.log('Success', user.data.signupUser.token)
			localStorage.setItem('graphcoolToken', user.data.signupUser.token)
			this.props.history.push('/user-dashboard')
		} catch (e) {
			console.error(`An error occured: `, e)
		}
	}

	handleChange = event => {
		const target = event.target
		const value = target.type === 'checkbox' ? target.checked : target.value
		const name = target.name

		this.setState({
			[name]: value
		})
    }
    
	render() {
		const { firstname, lastname, email, isUser, isProvider } = this.state
		if (this.props.loggedInUserQuery.loading) {
			return <div>Loading</div>
		}
		return (
			<FormContainer onSubmit={this.signupUser}>
				I am a User<input
					label="I'm a User."
					type="checkbox"
					name="user"
                    checked={isUser}
                
				/>
                I am a Provider
				<input
					label="I'm a Provider"
					type="checkbox"
					name="provider"
					checked={isProvider}
				/>
				<div>
					<input
						value={firstname}
						onChange={this.handleChange}
						label="First Name"
						type="text"
						name="firstname"
						margin="normal"
						placeholder="First"
						key="firstname"
					/>
					<input
						onChange={this.handleChange}
						value={lastname}
						label="Last Name"
						type="text"
						name="lastname"
						margin="normal"
						variant="outlined"
						placeholder="Last"
					/>
				</div>
				<input
					onChange={this.handleChange}
					value={email}
					label="Email"
					type="text"
					name="email"
					autoComplete="email"
					placeholder="email"
				/>
				<input
					onChange={this.handleChange}
					name="password"
					id="outlined-password-input"
					label="Password"
					type="password"
					autoComplete="current-password"
					margin="normal"
					variant="outlined"
				/>
				<label
					onChange={this.handleChange}
					name="agree"
					label="I agree to Terms and Conditions."
				/>
				<button onClick={this.signupUser}>Get Started</button>
			</FormContainer>
		)
	}
}

const SIGNUP_USER_MUTATION = gql`
	mutation signupUser(
		$email: String!
		$password: String!
		$firstname: String!
		$lastname: String!
        $isProvider: Boolean!
	) {
		signupUser(
			email: $email
			password: $password
			firstname: $firstname
			lastname: $lastname
            isProvider: $isProvider
		) {
			id
			token
		}
	}
`

const LOGGED_IN_USER_QUERY = gql`
	query LoggedInUserQuery {
		loggedInUser {
			id
		}
	}
`

export default compose(
	graphql(SIGNUP_USER_MUTATION, { name: 'signupUserMutation' }),
	graphql(LOGGED_IN_USER_QUERY, {
		name: 'loggedInUserQuery',
		options: { fetchPolicy: 'network-only' }
	})
)(withRouter(CreateUser))

import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'
import swal from 'sweetalert2'

class CreateLogin extends Component {
	state = {
		email: '',
		password: ''
	}

	componentDidMount() {
		localStorage.getItem('graphcoolToken')
	}

	render() {
		const { loggedInUserQuery = {}, classes } = this.props
		const { loading = true, loggedInUser } = loggedInUserQuery

		if (loading) {
			return <div>Loading</div>
		}

		if (loggedInUser && loggedInUser.id) {
			this.props.history.replace('/')
		}

		return (
			<div className="w-100 pa4 flex justify-center">
				<div style={{ maxWidth: 400 }} className="">
					<input
						value={this.state.email}
						placeholder="Email"
						onChange={e => this.setState({ email: e.target.value })}
						label="Email"
						type="text"
						name="email"
						autoComplete="email"
					/>
					<input
						label="Email"
						type="text"
						name="email"
						autoComplete="email"
						type="password"
						value={this.state.password}
						placeholder="Password"
						onChange={e =>
							this.setState({ password: e.target.value })
						}
					/>

					<button onClick={this.authenticateUser}>Log in</button>
				</div>
			</div>
		)
	}

	authenticateUser = async e => {
		e.preventDefault()
		e.stopPropagation()
		try {
			const { email, password } = this.state

			const response = await this.props.authenticateUserMutation({
				variables: { email, password }
			})
			localStorage.setItem(
				'graphcoolToken',
				response.data.authenticateUser.token
			)
			setTimeout(() => {
				this.props.history.replace('/')
			}, 1000)
		} catch (exception) {
			const { graphQLErrors: Errors } = exception
			if (!!Errors && !!Errors.length) {
				swal({
					title: 'An Error Occured.',
					text:
						(typeof Errors[0].functionError === 'string' &&
							Errors[0].functionError) ||
						(typeof Errors[0].functionError === 'object' &&
							Errors[0].functionError.hasOwnProperty('message') &&
							Errors[0].functionError.message) ||
						'Something went wrong.',
					type: 'error'
				})
			} else swal('An error occured.', exception.message, 'error')
		}
	}
}

const AUTHENTICATE_USER_MUTATION = gql`
	mutation AuthenticateUserMutation($email: String!, $password: String!) {
		authenticateUser(email: $email, password: $password) {
			token
		}
	}
`

const LOGGED_IN_USER_QUERY = gql`
	query LoggedInUserQuery {
		loggedInUser {
			id
		}
	}
`
export default compose(
	graphql(AUTHENTICATE_USER_MUTATION, { name: 'authenticateUserMutation' }),
	graphql(LOGGED_IN_USER_QUERY, {
		name: 'loggedInUserQuery',
		options: { fetchPolicy: 'network-only' }
	})
)(withRouter(CreateLogin))
